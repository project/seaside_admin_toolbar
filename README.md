# SEASIDE ADMIN TOOLBAR

The accompanying toolbar module to the Seaside Admin theme.

## MAINTAINER

- Xavier Mirabelli-Montan (xaviemirmon) - <https://www.drupal.org/u/xaviemirmon>
